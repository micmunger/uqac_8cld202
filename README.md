# <b>UQAC 8CLD202 - Déploiement d’une infrastructure IoT minimale sur le cloud</b>

## Objectifs
- Déployer 3 microservices sur le cloud: une interface web sur un conteneur, un conteneur avec une base de donnée et un conteneur pour des calculs de statistiques.
- Développer une API web RESTful et une API pour la communication interservices.
- Utiliser Azure DevOps pour le CI/CD pipeline.
- Contraster un déploiement sur Docker et sur Kubernetes
- Utiliser une machine virtuelle pour les conteneurs web et la bdd et une autre pour le conteneur de calculs

## Membres
- Alexandre Davy 
- Michael Munger
